package com.upis.upis.mobile;

import java.io.*;

public class TesteSerializacao {
    public static void main(String[] args) {
        Horario horario = new Horario();
        try {
            // Serialização
            FileOutputStream fileOut = new FileOutputStream("horario.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(horario);
            out.close();
            fileOut.close();

            // Desserialização
            FileInputStream fileIn = new FileInputStream("horario.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Horario horarioDesserializado = (Horario) in.readObject();
            in.close();
            fileIn.close();

            System.out.println("Hora: " + horarioDesserializado.getHora() + ", Minuto: " + horarioDesserializado.getMinuto());
        } catch (IOException | ClassNotFoundException i) {
            i.printStackTrace();
        }
    }
}
