package com.upis.upis.mobile;

public class HorarioNG implements IHorario{
    private int segundos;

    public HorarioNG(int segundos) {
        if (segundos < 0 || segundos > 86399) {
            throw new IllegalArgumentException("Segundos devem estar no intervalo [0, 86399].");
        }
        this.segundos = segundos;
    }
    @Override
    public byte getHora() {
        return (byte) (segundos / 3600);
    }
    @Override
    public void setHora(byte hora) {
        if (hora < 0 || hora > 23) {
            throw new IllegalArgumentException("Hora deve estar no intervalo [0, 23].");
        }
        segundos -= getHora() * 3600;
        segundos += hora * 3600;
    }
    @Override
    public byte getMinuto() {
        return (byte) ((segundos / 60) % 60);
    }
    @Override
    public void setMinuto(byte minuto) {
        if (minuto < 0 || minuto > 59) {
            throw new IllegalArgumentException("Minuto deve estar no intervalo [0, 59].");
        }
        segundos -= getMinuto() * 60;
        segundos += minuto * 60;
    }
    @Override
    public byte getSegundo() {
        return (byte) (segundos % 60);
    }
    @Override
    public void setSegundo(byte segundo) {
        if (segundo < 0 || segundo > 59) {
            throw new IllegalArgumentException("Segundo deve estar no intervalo [0, 59].");
        }
        segundos -= getSegundo();
        segundos += segundo;
    }
    @Override
    public void incrementaSegundo() {
        segundos++;
        if (ehUltimoSegundo()) {
            setHora((byte) 0);
        }
    }
    @Override
    public void incrementaMinuto() {
        segundos += 60;
        if (getMinuto() == 0) {
            incrementaHora();
        }
    }
    @Override
    public void incrementaHora() {
        segundos += 3600;
        if (getHora() == 24) {
            setHora((byte) 0);
        }
    }
    @Override
    public boolean ehUltimoSegundo() {
        return segundos == 86399;
    }
    @Override
    public boolean ehPrimeiroSegundo() {
        return segundos == 0;
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", getHora(), getMinuto(), getSegundo());
    }
}
