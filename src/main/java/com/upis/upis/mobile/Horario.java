package com.upis.upis.mobile;

import java.io.Serializable;

public class Horario implements IHorario, Serializable {

	private byte hora; 		
	private byte minuto; 	
	private byte segundo; 	
	
	public Horario(){
		setSegundo((byte)0);
	}
	public Horario(byte hora, byte minuto, byte segundo){
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}
	
	public Horario(Horario horario) {
		this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
	}

	public void setHora(byte hora) {
		
		if(hora >= 0 && hora <= 23) {
			this.hora = hora;
		}
	}
	
	public byte getHora() {
		return this.hora;
	}		

	public void setMinuto(byte minuto) {
		if(minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
		}
	}
	
	public byte getMinuto() {
		return this.minuto;
	}
	
	public void setSegundo(byte segundo) {
		if(segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}
	
	public byte getSegundo() {
		return this.segundo;
	}
	
	public String toString() {
		return getHora() + ":" + getMinuto() + ":" + getSegundo();
	}
	
	public void incrementaSegundo() {
		
		byte s = (byte)(segundo + 1);
		
		if(s == 60) {
			segundo = 0;
			incrementaMinuto();
		}else {
			segundo = s;
		}
	}
	
	public void incrementaMinuto() {
		byte m = (byte)(minuto + 1);
		
		if(m == 60) {
			minuto = 0;
			incrementaHora();
		}else {
			minuto = m;
		}
	}

	public void incrementaHora() {
		byte h = (byte)(hora + 1);
		
		if(h == 24) {
			hora = 0;
		}else {
			hora = h;
		}			
	}

	@Override
	public boolean ehUltimoSegundo() {
		return false;
	}

	@Override
	public boolean ehPrimeiroSegundo() {
		return false;
	}

	public void incrementaSegundosEmN(int n) {
			for(int i =0; i < n; i++) {
				incrementaSegundo();
			}
	}

	public void incrementaMinutoEmN(int n) {
			for(int i =0; i < n; i++) {
				incrementaMinuto();
			}
	}
	
	public void incrementaHoraEmN(int n) {
			for(int i =0; i < n; i++) {
				incrementaHora();
			}
	}


	private boolean equalsHora(Horario hr) {
		return this.getHora() == hr.getHora();
	}

	private boolean equalsSegundo(Horario hr) {
		return this.getSegundo() == hr.getSegundo();
	}
	
	private boolean equalsMinuto(Horario hr) {
		return this.getMinuto() == hr.getMinuto();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hora;
		result = prime * result + minuto;
		result = prime * result + segundo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horario other = (Horario) obj;
		if (hora != other.hora)
			return false;
		if (minuto != other.minuto)
			return false;
		if (segundo != other.segundo)
			return false;
		return true;
	}
	
	public boolean menor(Horario hr) {
		
		if(this.getHora() < hr.getHora()) {
			return true;
		}
		
		if(equalsHora(hr) && this.getMinuto() < hr.getMinuto()){
			return true;
		}
		
		if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() < hr.getSegundo()){
			return true;
		}
		
		return false;
	}
	
	public boolean maior(Horario hr) {
		
		if(this.getHora() > hr.getHora()) {
			return true;
		}
		
		if(equalsHora(hr) && this.getMinuto() > hr.getMinuto()){
			return true;
		}
		
		if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() > hr.getSegundo()){
			return true;
		}
		
		return false;
	}
	
	public boolean menorOuIgual(Horario hr) {
		if(menor(hr) || equalsHora(hr)) {
			return true;
		}
		return false;
	}
	
	
	
	
	

}





