package com.upis.upis.mobile;

public class HorarioNGTeste {
    public static void main(String[] args) {
        HorarioNG h = new HorarioNG(3600); // Cria horário com 1 hora (3600 segundos)
        System.out.println(h); // Saída: "01:00:00"
        h.incrementaMinuto(); // Incrementa 1 minuto
        System.out.println(h); // Saída: "01:01:00"
        h.setSegundo((byte) 30); // Ajusta os segundos para 30
        System.out.println(h); // Saída: "01:01:30"
    }
}
