package com.upis.upis;

import java.awt.image.BufferedImage;
import java.io.*;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import javax.imageio.ImageIO;

@WebServlet(name = "helloServlet", value = "/hello")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (OutputStream out = response.getOutputStream()) {
            // Obtém o objeto ServletContext
            ServletContext context = request.getServletContext();

            // Obtém o caminho relativo do arquivo
            String relativePath = "image.jpeg";
            String absolutePath = context.getRealPath(relativePath);

            // Envia a imagem no corpo da resposta
            File imageFile = new File(absolutePath);
            BufferedImage image = ImageIO.read(imageFile);
            response.setContentType("image/jpeg");
            response.setContentLength((int) imageFile.length());
            ImageIO.write(image, "jpeg", out);
        }catch (IOException e){
            response.getWriter().println("Não foi possivel carregar imagem!!!");
        }
    }

    public void destroy() {
    }
}