package com.upis.upis;

public class ProdutorConsumidor {

    private static final int TAM_BUFFER = 10;
    private static int[] buffer = new int[TAM_BUFFER];
    private static int indexProd = 0;
    private static int indexCons = 0;

    public static void main(String[] args) {
        // Implementação não sincronizada
        Thread produtor = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                buffer[indexProd] = i;
                indexProd = (indexProd + 1) % TAM_BUFFER;
            }
        });
        Thread consumidor = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(buffer[indexCons]);
                indexCons = (indexCons + 1) % TAM_BUFFER;
            }
        });
        produtor.start();
        consumidor.start();

        // Implementação sincronizada
        Object monitor = new Object();
        Thread produtorSync = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                synchronized (monitor) {
                    while ((indexProd + 1) % TAM_BUFFER == indexCons) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    buffer[indexProd] = i;
                    indexProd = (indexProd + 1) % TAM_BUFFER;
                    monitor.notifyAll();
                }
            }
        });
        Thread consumidorSync = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                synchronized (monitor) {
                    while (indexCons == indexProd) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("consumidorSync: " + buffer[indexCons]);
                    indexCons = (indexCons + 1) % TAM_BUFFER;
                    monitor.notifyAll();
                }
            }
        });
        produtorSync.start();
        consumidorSync.start();
    }
}

